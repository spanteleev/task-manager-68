package ru.tsc.panteleev.tm.dto.request.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class DataJsonJaxbLoadRequest extends AbstractUserRequest {

    public DataJsonJaxbLoadRequest(@Nullable String token) {
        super(token);
    }

}
