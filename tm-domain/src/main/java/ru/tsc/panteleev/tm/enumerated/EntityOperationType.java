package ru.tsc.panteleev.tm.enumerated;

public enum EntityOperationType {

    POST_LOAD,
    POST_PERSIST,
    POST_UPDATE,
    POST_REMOVE

}
