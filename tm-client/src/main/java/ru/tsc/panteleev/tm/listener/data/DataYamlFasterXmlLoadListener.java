package ru.tsc.panteleev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.data.DataYamlFasterXmlLoadRequest;
import ru.tsc.panteleev.tm.event.ConsoleEvent;

@Component
public class DataYamlFasterXmlLoadListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Load data from yaml file";

    @NotNull
    public static final String NAME = "data-load-yaml-fasterxml";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataYamlFasterXmlLoadListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        showDescription();
        getDomainEndpoint().loadDataYamlFasterXml(new DataYamlFasterXmlLoadRequest(getToken()));
    }

}
