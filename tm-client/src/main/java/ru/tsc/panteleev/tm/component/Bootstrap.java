package ru.tsc.panteleev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.api.service.*;
import ru.tsc.panteleev.tm.event.ConsoleEvent;
import ru.tsc.panteleev.tm.util.SystemUtil;
import ru.tsc.panteleev.tm.util.TerminalUtil;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap {

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @NotNull
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void runStartupOperation() {
        initPID();
        fileScanner.start();
        registryShutdownHookOperation();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
    }

    private void registryShutdownHookOperation() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
                fileScanner.stop();
            }
        });
    }

    public void run(@Nullable String[] args) {
        if (runWithArgument(args))
            System.exit(0);
        runStartupOperation();
        while (true)
            runWithCommand();
    }

    public void runWithCommand() {
        try {
            System.out.println("ENTER COMMAND:");
            String command = TerminalUtil.nextLine();
            applicationEventPublisher.publishEvent(new ConsoleEvent(command));
            loggerService.command(command);
        } catch (final Exception e) {
            loggerService.error(e);
        }
    }

    public boolean runWithArgument(@Nullable String[] args) {
        if (args == null || args.length == 0) return false;
        applicationEventPublisher.publishEvent(new ConsoleEvent(args[0]));
        return true;
    }

}
