package ru.tsc.panteleev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.tsc.panteleev.tm.event.ConsoleEvent;
import ru.tsc.panteleev.tm.util.TerminalUtil;

@Component
public class TaskRemoveByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskRemoveByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskEndpoint().removeTaskById(new TaskRemoveByIdRequest(getToken(), id));
    }

}
