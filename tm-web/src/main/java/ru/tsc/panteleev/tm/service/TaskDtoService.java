package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.panteleev.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.panteleev.tm.dto.model.TaskDto;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Service
public class TaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository taskRepository;

    public void add(@NotNull final TaskDto task) {
        taskRepository.saveAndFlush(task);
    }

    public void create() {
        @NotNull final TaskDto task = new TaskDto();
        task.setName("Task " + System.currentTimeMillis() / 1000);
        task.setDescription(UUID.randomUUID().toString());
        taskRepository.saveAndFlush(task);
    }

    public boolean existsById(@NotNull final String id) {
        return taskRepository.existsById(id);
    }

    @Nullable
    public TaskDto findById(@NotNull final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Nullable
    public Collection<TaskDto> findAll() {
        return taskRepository.findAll();
    }

    public long count() {
        return taskRepository.count();
    }

    public void save(@NotNull final TaskDto task) {
        taskRepository.saveAndFlush(task);
    }

    public void deleteById(@NotNull final String id) {
        taskRepository.deleteById(id);
    }

    public void delete(@NotNull final TaskDto task) {
        taskRepository.delete(task);
    }

    public void deleteAll(@NotNull final List<TaskDto> taskList) {
        taskRepository.deleteAll(taskList);
    }

    public void clear() {
        taskRepository.deleteAll();
    }

}

