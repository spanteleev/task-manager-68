package ru.tsc.panteleev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.panteleev.tm.api.endpoint.IProjectDtoEndpoint;
import ru.tsc.panteleev.tm.dto.model.ProjectDto;
import ru.tsc.panteleev.tm.service.ProjectDtoService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.tsc.panteleev.tm.api.endpoint.IProjectDtoEndpoint")
public class ProjectEndpointImpl implements IProjectDtoEndpoint {

    @NotNull
    @Autowired
    private ProjectDtoService projectDtoService;

    @Override
    @WebMethod
    @PutMapping("/create")
    public void create() {
        projectDtoService.create();
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return projectDtoService.existsById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return projectDtoService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<ProjectDto> findAll() {
        return projectDtoService.findAll();
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return projectDtoService.count();
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public void save(
            @NotNull
            @WebParam(name = "project")
            @RequestBody final ProjectDto project
    ) {
        projectDtoService.save(project);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "project")
            @RequestBody final ProjectDto project
    ) {
        projectDtoService.delete(project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        projectDtoService.deleteById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "projects")
            @RequestBody final List<ProjectDto> projects
    ) {
        projectDtoService.deleteAll(projects);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        projectDtoService.clear();
    }

}

