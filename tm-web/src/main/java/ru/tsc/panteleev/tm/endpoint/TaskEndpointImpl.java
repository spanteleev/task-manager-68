package ru.tsc.panteleev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.panteleev.tm.api.endpoint.ITaskDtoEndpoint;
import ru.tsc.panteleev.tm.dto.model.TaskDto;
import ru.tsc.panteleev.tm.service.TaskDtoService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.tsc.panteleev.tm.api.endpoint.ITaskDtoEndpoint")
public class TaskEndpointImpl implements ITaskDtoEndpoint {

    @NotNull
    @Autowired
    private TaskDtoService taskDtoService;

    @Override
    @WebMethod
    @PutMapping("/create")
    public void create() {
        taskDtoService.create();
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return taskDtoService.existsById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return taskDtoService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<TaskDto> findAll() {
        return taskDtoService.findAll();
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return taskDtoService.count();
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public void save(
            @NotNull
            @WebParam(name = "task")
            @RequestBody final TaskDto task
    ) {
        taskDtoService.save(task);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "task")
            @RequestBody final TaskDto task
    ) {
        taskDtoService.delete(task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        taskDtoService.deleteById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "tasks")
            @RequestBody final List<TaskDto> tasks
    ) {
        taskDtoService.deleteAll(tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        taskDtoService.clear();
    }

}

