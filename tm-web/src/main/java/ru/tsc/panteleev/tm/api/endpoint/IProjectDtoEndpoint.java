package ru.tsc.panteleev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.tsc.panteleev.tm.dto.model.ProjectDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface IProjectDtoEndpoint {

    @WebMethod
    @PutMapping("/create")
    void create();

    @WebMethod
    @GetMapping("/existById/{id}")
    boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/findById/{id}")
    ProjectDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/findAll")
    Collection<ProjectDto> findAll();

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/save")
    void save(
            @NotNull
            @WebParam(name = "project")
            @RequestBody ProjectDto project
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @NotNull
            @WebParam(name = "project")
            @RequestBody ProjectDto project
    );

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @NotNull
            @WebParam(name = "projects")
            @RequestBody List<ProjectDto> projects
    );

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

}


