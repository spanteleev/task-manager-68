package ru.tsc.panteleev.tm.configuration;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.tsc.panteleev.tm.dto.model.AbstractModelDto;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.tsc.panteleev.tm")
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("ru.tsc.panteleev.tm.api.repository.dto")
public class ApplicationConfiguration {

    @Bean
    @NotNull
    public DataSource dataSource(
            @NotNull @Value("#{environment['database.driver']}") final String databaseDriver,
            @NotNull @Value("#{environment['database.url']}") final String databaseConnectionString,
            @NotNull @Value("#{environment['database.username']}") final String databaseUser,
            @NotNull @Value("#{environment['database.password']}") final String databasePassword
    ) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseDriver);
        dataSource.setUrl(databaseConnectionString);
        dataSource.setUsername(databaseUser);
        dataSource.setPassword(databasePassword);
        return dataSource;
    }


    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource,
            @NotNull @Value("#{environment['database.sql_dialect']}") final String dialect,
            @NotNull @Value("#{environment['database.hbm2dll_auto']}") final String hbm2Ddl,
            @NotNull @Value("#{environment['database.show_sql']}") final String showSql,
            @NotNull @Value("#{environment['database.cache.use_second_level_cache']}") final String useSecondLvlCache,
            @NotNull @Value("#{environment['database.cache.provider_configuration_file_resource_path']}") final String cacheProviderConfigFile,
            @NotNull @Value("#{environment['database.cache.region.factory_class']}") final String cacheRegionFactoryClass,
            @NotNull @Value("#{environment['database.cache.use_query_cache']}") final String cacheUseQueryCache,
            @NotNull @Value("#{environment['database.cache.use_minimal_puts']}") final String cacheUseMinPuts,
            @NotNull @Value("#{environment['database.cache.region_prefix']}") final String cacheRegionPrefix
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan(
                AbstractModelDto.class.getPackage().getName()
        );
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, dialect);
        properties.put(Environment.HBM2DDL_AUTO, hbm2Ddl);
        properties.put(Environment.SHOW_SQL, showSql);
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, useSecondLvlCache);
        properties.put(Environment.CACHE_PROVIDER_CONFIG, cacheProviderConfigFile);
        properties.put(Environment.CACHE_REGION_FACTORY, cacheRegionFactoryClass);
        properties.put(Environment.USE_QUERY_CACHE, cacheUseQueryCache);
        properties.put(Environment.USE_MINIMAL_PUTS, cacheUseMinPuts);
        properties.put(Environment.CACHE_REGION_PREFIX, cacheRegionPrefix);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }


    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}
