package ru.tsc.panteleev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.tsc.panteleev.tm.dto.model.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskDtoEndpoint {

    @WebMethod
    @PutMapping("/create")
    void create();

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/findById/{id}")
    TaskDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/findAll")
    Collection<TaskDto> findAll();

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/save")
    void save(
            @NotNull
            @WebParam(name = "task")
            @RequestBody TaskDto task
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @NotNull
            @WebParam(name = "task")
            @RequestBody TaskDto task
    );

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @NotNull
            @WebParam(name = "tasks")
            @RequestBody List<TaskDto> tasks
    );

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

}

